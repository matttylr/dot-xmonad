import XMonad
import XMonad.Config.Gnome
import XMonad.Util.EZConfig
import XMonad.Actions.CycleWS
import Text.Regex.Posix ((=~))

q ~? x = fmap (=~ x) q 

myManageHook = composeAll (
  [ manageHook gnomeConfig
  , className =? "Unity-2d-panel" --> doIgnore
  , className =? "Do" --> doIgnore
  , className ~? "Gimp.*" --> doFloat
  , className =? "Calendar" --> doFloat
  , className =? "Thunderbird" --> doShift "4"
  , className =? "Msgcompose" --> doFloat
  , className =? "Gnome-terminal" --> doShift "1"
  , className =? "Emacs" --> doShift "2"
  , className =? "Firefox" --> doShift "3"
  -- , className =? "rdesktop" --> doShift "4"
  , className =? "Pidgin" --> doShift "5"
  , className =? "Clementine" --> doShift "6"
  ])

main = xmonad $ gnomeConfig { manageHook = myManageHook }
       { modMask = mod4Mask -- set the mod key to the windows key
       , terminal = "konsole"
       }
       `additionalKeysP`
       [ ("M-p",   spawn "dmenu_run -b")
       , ("M-w",   spawn "surf")
       , ("M-c",   spawn "rdesktop -u mtaylor -d CMEDGROUP -K -f 10.1.0.24")
       , ("M-x",   spawn "xkill")
       , ("M-i",   prevWS)
       , ("M-o",   nextWS)
       , ("S-M-i", shiftToPrev >> prevWS)
       , ("S-M-o", shiftToNext >> nextWS)
       , ("M-z",   toggleWS)
       , ("M-f",   shiftTo Next EmptyWS)  -- find a free workspace
       , ("M-d",   moveTo Next EmptyWS)  -- move to a free workspace
       ]
